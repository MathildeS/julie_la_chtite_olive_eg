var map = L.map('mapid').setView([46.52863469527167,2.43896484375], 6);
// Chargement de la carte google satellite
var map2 = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
   maxZoom: 20,
   subdomains:['mt0','mt1','mt2','mt3']
}).addTo(map);


var ObjetFixe = L.Icon.extend({
  options: {
  iconSize: [40, 40],
  iconAnchor: null,
  popupAnchor: [4, 4],
  maxZoom : 15,
  }
});

// Création des icônes des lieux
olivier = new ObjetFixe({iconUrl: 'lieux/olivier.png'});
mont_blanc = new ObjetFixe({iconUrl: 'lieux/sommet_mt_blanc.png'});
mont_lure = new ObjetFixe({iconUrl: 'lieux/montagne_lure.png'});
digoin = new ObjetFixe({iconUrl: 'lieux/chapiteau.png'});
strasbourg = new ObjetFixe({iconUrl: 'lieux/eglise.png'});
nord = new ObjetFixe({iconUrl: 'lieux/mine_charbon.png'});
bretagne = new ObjetFixe({iconUrl: 'lieux/carriere.png'});
paris = new ObjetFixe({iconUrl: 'lieux/tour_eiffel.png'});
bordeaux = new ObjetFixe({iconUrl: 'lieux/pat.png'});
sancy = new ObjetFixe({iconUrl: 'lieux/volcan.png'});

// Création des icônes des objets
gentiane = new ObjetFixe({iconUrl: 'objets/gentiane.png'});
coffre = new ObjetFixe({iconUrl: 'objets/coffre.png'});
mont_blanc_gateau = new ObjetFixe({iconUrl: 'objets/mont_blanc.png'});
bretzel = new ObjetFixe({iconUrl: 'objets/bretzel.png'});
charbon = new ObjetFixe({iconUrl: 'objets/charbon.png'});
gateau_breton = new ObjetFixe({iconUrl: 'objets/gateau_breton.jpg'});
opera = new ObjetFixe({iconUrl: 'objets/opera.jpg'});
canelet = new ObjetFixe({iconUrl: 'objets/canele_png.png'});
tire_bouchon = new ObjetFixe({iconUrl: 'objets/tire_bouchon.png'});
vin = new ObjetFixe({iconUrl: 'objets/vin.png'});
escargot = new ObjetFixe({iconUrl: 'objets/escargot.jpg'});
mont_dor = new ObjetFixe({iconUrl: 'objets/mont_dor.jpg'});
julie_mure = new ObjetFixe({iconUrl: 'objets/julie_mure.png'});



// Création des marqueurs des lieux
olivier = L.marker([43.959933,5.780712], {icon: olivier}).addTo(map);
mont_blanc = L.marker([45.832622,6.865175], {icon: mont_blanc});//.addTo(map);
mont_lure = L.marker([44.1231,5.8026], {icon: mont_lure});//.addTo(map);
digoin = L.marker([46.481976,3.984840], {icon: digoin});//.addTo(map);
strasbourg = L.marker([48.581880,7.751035], {icon: strasbourg});//.addTo(map);
nord = L.marker([50.460844, 2.986978], {icon: nord});//.addTo(map);
bretagne = L.marker([48.609539,-2.494735], {icon: bretagne});//.addTo(map);
tour_eiffel = L.marker([48.858370,2.294481], {icon: paris});//.addTo(map);
bordeaux = L.marker([44.840118,-0.598408], {icon: bordeaux});//.addTo(map);
sancy = L.marker([45.528387,2.813930], {icon: sancy});//.addTo(map);

// Création des marqueurs des objets
gentiane = L.marker([44.1231,5.8026], {icon: gentiane});
coffre1 = L.marker([45.832622,6.865175], {icon: coffre});
mont_blanc_gateau = L.marker([45.832622,6.865175], {icon: mont_blanc_gateau});
bretzel = L.marker([48.581880,7.751035], {icon: bretzel});
coffre2 = L.marker([50.460844, 2.986978], {icon: coffre});
charbon = L.marker([50.460844, 2.986978], {icon: charbon});
gateau_breton = L.marker([48.609539,-2.494735], {icon: gateau_breton});
opera = L.marker([48.858370,2.294481], {icon: opera});
coffre3 = L.marker([44.840118,-0.598408], {icon: coffre});
canelet = L.marker([44.840118,-0.598408], {icon: canelet});
tire_bouchon = L.marker([44.840118,-0.598408], {icon: tire_bouchon});
vin = L.marker([46.481976,3.984840], {icon: vin});
escargot = L.marker([46.481976,3.984840], {icon: escargot});
mont_dor = L.marker([45.528387,2.813930], {icon: mont_dor});
coffre4 = L.marker([45.528387,2.813930], {icon: coffre});
julie_mure = L.marker([43.959933,5.780712], {icon: julie_mure});


// Création de deux groupes de couches : les lieux et les objets
var layerLieux = L.layerGroup();
var layerObjets = L.layerGroup();

var paraind = document.getElementById('paraind');
var form = document.getElementById('valid_finale');

function ajoutLieuLayer(lieu){
  layerLieux.addLayer(lieu);
}

function retireObjetLayer(objet){
  layerObjets.removeLayer(objet);
}
function ajoutObjetLayer(objet){
  layerObjets.addLayer(objet);
}

// Fonction pour rentrer les réponses pour les codes des coffres par exemple
function repondre(enigme, valeur){
  var suggestion = prompt("<p>" + enigme + "</p><p>Veuillez rentrer votre réponse :</p>");
  while (true){
    if (suggestion != valeur) {
      suggestion = prompt('Raté, veuillez réessayer !');
    }else {
      alert('bonne réponse');
      break;
    }
  }
}



olivier.addEventListener("click", function(){
  ajoutLieuLayer(mont_lure);
})


map.on('zoomend', function(){
  // Fonction zoom qui gère l'apparition des lieux et objets en Fonction
  // du zoom
  var zoomlevel = map.getZoom();
  if (zoomlevel < 10){
    if (map.hasLayer(layerLieux)) {
      map.removeLayer(layerLieux)
    }else {
      console.log("no");
      console.log(zoomlevel);
    }
    if (map.hasLayer(layerObjets)) {
      map.removeLayer(layerObjets)
    }else {
      console.log("no");
      console.log(zoomlevel);
    }
  }
  if (zoomlevel>=10) {
    console.log(zoomlevel);
    if (map.hasLayer(layerLieux)) {
      console.log("lieux already add");
    }else {
      layerLieux.addTo(map);
    }
    if (map.hasLayer(layerObjets)) {
      console.log("objets already add");
    }else {
      layerObjets.addTo(map);
    }
  }

// Succession d'écoute sur les différents lieux et objets pour faire apparaitre
// les lieux et objets après intéractions sur la map et dans l'inventaire
  mont_lure.addEventListener("click", function(){
    ajoutObjetLayer(gentiane);
  })

  gentiane.addEventListener("click", function(){
    var test = true;
    var popupContent = '<p>H : Oh mais il y a là une belle gentiane ! Je vais la ramener à Julie ! Ah mais il y a un petit parchemin sous la gentiane…qu’y a-t-il décrit dessus ?</p><p>"Pour voir plus loin, il faut aller sur le toit de la France"</p>';
    gentiane.bindPopup(popupContent).openPopup();
    ajoutLieuLayer(mont_blanc);
    paraind.innerHTML = "Pour voir plus loin, il faut aller sur le toit de la France";
    // Ajout dans l'inventaire après clique
    var bloc1 = document.getElementById("bloc1");
    bloc1.innerHTML =  "<img id='gentiane' src='objets/gentiane.png' alt='' height='50px' width='50px'/>";
 })


 mont_blanc.addEventListener("click", function(){
   ajoutObjetLayer(coffre1);
  })
})

 coffre1.addEventListener("click", function(){
   var enigme = "Enigme du coffre: A quelle hauteur suis-je ? ";
   var valeur = 4810;
   var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
   while (true){
     if (suggestion != valeur) {
       suggestion = prompt('Raté, veuillez réessayer !');
     }else {
       alert('bonne réponse');
       ajoutObjetLayer(mont_blanc_gateau);
       break;
     }
   }

 })

mont_blanc_gateau.addEventListener('click', function(){
  paraind.innerHTML = "Quand on a une vue très perçante, on peut apercevoir l'unique clocher qui culmine à 141m de cette ville siège de discussions parlementaires dans le grand-est.";
  var popupContent = "<p>Voici l'indice suivant inscrit au fond du coffre :</p><p>Quand on a une vue très perçante, on peut apercevoir mon unique clocher qui culmine à 141m.</p>";
  mont_blanc_gateau.bindPopup(popupContent).openPopup();
  ajoutLieuLayer(strasbourg);
  var bloc2 = document.getElementById("bloc2");
  bloc2.innerHTML =  "<img id='mont_blc' src='objets/mont_blanc.png' alt='' height='50px' width='50px'/>";
})

 strasbourg.addEventListener('click', function(){
   ajoutObjetLayer(bretzel);
 })

 bretzel.addEventListener('click', function(){
   var popupContent = '<p>En haut du clocher Hatlas croise un coléoptère.</p><p>C : Bien le bonjour à toi papillon, si tu cherches un endroit à visiter je te conseillerais cette destination !</p><p>Le coléoptère donne à Hatlas un parchemin sur lequel il est écrit :</p><p>Pour une destination de rêve, se rendre dans les profondeurs au nord de la France à l’endroit où les hommes ont extrait pour la dernière fois les entrailles de la Terre dans cette région.</p>';
   bretzel.bindPopup(popupContent).openPopup();
   ajoutLieuLayer(nord);
   paraind.innerHTML = "Pour une destination de rêve, se rendre dans les profondeurs au nord de la France à l’endroit où les hommes ont extrait pour la dernière fois les entrailles de la Terre dans cette région.";
   var bloc3 = document.getElementById("bloc3");
   bloc3.innerHTML =  "<img id='bretzel' src='objets/bretzel.png' alt='' height='50px' width='50px'/>";
 })

 nord.addEventListener('click', function(){
   ajoutObjetLayer(coffre2);
 })

 coffre2.addEventListener('click', function(){
   var enigme = "Enigme du coffre: En quelle année, la mine d'Oignies a-t-elle été fermée ? ";
   var valeur = 1990;
   var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
   while (true){
     if (suggestion != valeur) {
       suggestion = prompt('Raté, veuillez réessayer !');
     }else {
       alert('bonne réponse');
       ajoutObjetLayer(charbon);
       break;
     }
   }
 })

charbon.addEventListener('click', function(){
  ajoutLieuLayer(bretagne);
  var popupContent = "<p>Dans ce coffre il y avait de beaux morceaux de charbons et une vieille lettre adressée à un mineur sur laquelle était inscrit :</p><p>Ma carrière est maintenant remplie de rose et non de noir comme la tienne... Je te conseille vivement de m'y rejoindre, l'air y est bien plus respirable et le temps y est plus changeant !</p>";
  charbon.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Ma carrière est maintenant remplie de rose et non de noir comme la tienne... Je vous conseille vivement de m'y rejoindre, l'air y est bien plus respirable et le temps y est plus changeant!";
  var bloc4 = document.getElementById("bloc4");
  bloc4.innerHTML =  "<img id='charbon' src='objets/charbon.png' alt='' height='50px' width='50px'/>";
})

bretagne.addEventListener('click', function(){
  ajoutObjetLayer(gateau_breton);
})

gateau_breton.addEventListener('click', function(){
  ajoutLieuLayer(tour_eiffel);
  var popupContent = "<p>Hatlas entend une conversation entre deux libellules :</p><p>L1 : Ce monument est le plus haut de Paris.</p><p>L2 : Oui, c’est le plus impressionnant que j’ai vu !</p><p>H : *Tiens, tiens, je crois que je vais me rendre à la capitale voir ce fameux monument*</p>";
  gateau_breton.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Rendez-vous sur le plus haut monument de la capitale française.";
  var bloc5 = document.getElementById("bloc5");
  bloc5.innerHTML =  "<img id='gateau_breton' src='objets/gateau_breton.jpg' alt='' height='50px' width='50px'/>";
})

tour_eiffel.addEventListener('click', function(){
  ajoutObjetLayer(opera);
  ajoutLieuLayer(bordeaux);
  var popupContent = "<p>Hatlas se rend en haut de la tour Eiffel et voit un couple manger un super gâteau.</p><p>F : Humm !!! Cet opéra est super bon !</p><p>Ho : Oui et ce petit vin était vraiment excellent !</p><p>F : Oui, il provient d’une ville très connue pour son vin !</p><p>H : *Hum, je vais emporter un bout de ce gâteau pour Julie et après je vais me rendre dans cette fameuse ville*</p>";
  tour_eiffel.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "N'oubliez pas de prendre l'opera puis rendez-vous dans une ville connue essentiellement pour son vin.";
  var bloc6 = document.getElementById("bloc6");
  bloc6.innerHTML =  "<img id='opera' src='objets/opera.jpg' alt='' height='50px' width='50px'/>";
})

bordeaux.addEventListener('click', function(){
  ajoutObjetLayer(coffre3);
})

coffre3.addEventListener('click', function(){
  var enigme = "Enigme du coffre: code INSEE de cette grande ville.";
  var valeur = 33063;
  var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
  while (true){
    if (suggestion != valeur) {
      suggestion = prompt('Raté, veuillez réessayer !');
    }else {
      alert('bonne réponse');
      ajoutObjetLayer(canelet);
      break;
    }
  }
  var bloc7 = document.getElementById("bloc7");
  bloc7.innerHTML =  "Cannele<img id='cannele' src='objets/canele_png.png' alt='' height='50px' width='50px'/>";

})

canelet.addEventListener('click', function(){
  ajoutObjetLayer(tire_bouchon);
})

tire_bouchon.addEventListener('click', function(){
  ajoutLieuLayer(digoin);
  var popupContent = "<p>Dans le coffre de la tenancière de l'épicierie, il y avait là de drôle de trouvailles pour notre cher Hatlas. Des canelés qu'elle gardait pour son goûter, un drôle d'engin que l'on appelle 'tire-bouchon' et un prospectus sur lequel trônait une grosse inscription :</p><p>Vous voulez découvrir un mets exceptionnel ? Pour cela rendez vous dans la ville où se déroule la plus grande fête du cousin de la limace.</p><p>Hatlas :*Quelle est donc ce drôle d'animal ?*</p>";
  tire_bouchon.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Vous voulez découvrir un mets exceptionnel ? Pour cela rendez vous dans la ville où se déroule la plus grande fête du cousin de la limace.";
  var bloc8 = document.getElementById("bloc8");
  bloc8.innerHTML =  "<img id='tire_bouchon' src='objets/tire_bouchon.png' alt='' height='50px' width='50px'/>";
})

digoin.addEventListener('click', function(){
  ajoutObjetLayer(escargot);
  ajoutObjetLayer(vin);
})

vin.addEventListener('click', function(){
  ajoutLieuLayer(sancy);
  var popupContent = "<p>Hatlas:*J'ai bien fait de prendre ce tire_bouchon à Bordeaux ainsi je pourrais déboucher cette bouteille! Je vais prendre aussi quelques escargots pour Julie.*</p><p>Hatlas : Il me reste de la place que pour un dernier souvenir et il faut que je reprenne de la hauteur pour voir le chemin pour rentrer. J’ai entendu dire que le point culminant de la chaîne montagneuse du centre de la France valait le détour !</p>";
  vin.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Hatlas : Il me reste de la place que pour un dernier souvenir et il faut que je reprenne de la hauteur pour voir le chemin pour rentrer. J’ai entendu dire que le point culminant de la chaîne montagneuse du centre de la France valait le détour !";
  var bloc9= document.getElementById("bloc9");
  bloc9.innerHTML =  "<img id='vin' src='objets/vin.png' alt='' height='50px' width='50px'/>";
  var bloc10 = document.getElementById("bloc10");
  bloc10.innerHTML =  "<img id='escargot' src='objets/escargot.jpg' alt='' height='50px' width='50px'/>";

})

sancy.addEventListener('click', function(){
  ajoutObjetLayer(coffre4);
})

coffre4.addEventListener('click', function(){
  var enigme = "Enigme du coffre: Quel est le nombre de volcans en Auvergne?";
  var valeur = 80;
  var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
  while (true){
    if (suggestion != valeur) {
      suggestion = prompt('Raté, veuillez réessayer !');
    }else {
      alert('bonne réponse');
      ajoutObjetLayer(mont_dor);
      break;
    }
  }
})

mont_dor.addEventListener('click', function(){
  ajoutLieuLayer(julie_mure);
  var popupContent = "<p>Hatlas: Il est maintenant temps de rentrer si je veux revenir avant l'olivaison. Direction le lieu où des élèves de l’ENSG arrivent en grand nombre vers la fin de l’année scolaire</p>";
  mont_dor.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Direction le lieu où des élèves de l’ENSG arrivent en grand nombre vers la fin de l’année scolaire";
  var bloc11 = document.getElementById("bloc11");
  bloc11.innerHTML =  "<img id='mont_dor' src='objets/mont_dor.jpg' alt='' height='50px' width='50px'/>";
})

julie_mure.addEventListener('click', function(){
    bloc1.innerHTML ="";
    bloc2.innerHTML ="";
    bloc3.innerHTML ="";
    bloc4.innerHTML ="";
    bloc5.innerHTML ="";
    bloc6.innerHTML ="";
    bloc7.innerHTML ="";
    bloc8.innerHTML ="";
    bloc9.innerHTML ="";
    bloc10.innerHTML ="";
    bloc11.innerHTML ="";
  // submit d'un formulaire invisible sur la page et qui renvoit vers la page de question finale
  form.submit();
})

// Fonction pour récupérer l'objet bloqué par le lieu pris en entrée
function lieuRecupObjet(val, lieuPrObjet, icon_objet_donnees, layerobjets){
  var data = "lieuPrObjet=" + val;

  fetch('carte.php',{
    method: 'post',
    body: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json()
)
  .then(r => {
//traitement à modifier:
    // Création des icônes des lieux
    charbon = new ObjetFixe({iconUrl: r[1]});
    console.log(r[1]);
    // Création des marqueurs objets
    charbon_m = L.marker([r[3],r[4]], {icon: charbon});//.addTo(map);

    charbon.addTo(map);
    //map.on('zoomend',zoom(charbon_m, r2[5]));

  })
};

// fonction pour récupérer le lieu bloqué par le lieu pris en entrée
function lieuRecupLieu(val, lieuPrLieu, icon_lieu_donnees, layerLieux){
  var data = {type: lieuPrLieu, demandeur: val};

  fetch('carte.php',{
    method: 'post',
    body: JSON.stringify(data)
    // headers: {
    //   'Content-Type': 'application/x-www-form-urlencoded'
    // }
  })
  .then(r => r.json()
  )
  .then(r => {
    icon_lieu_donnees.nom = r["nom"];
    icon_lieu_donnees.cheminacces = r["cheminacces"];
    icon_lieu_donnees.lat = r["lat"];
    icon_lieu_donnees.longi = r["longi"];
    icon_lieu_donnees.zoom = r["zoom"];
    icon_lieu_donnees.bloquepar = r["bloquepar"];
    icon_lieu_donnees.debloque = r["debloque"];

    var icone =new ObjetFixe({iconUrl: r["cheminacces"]});
    icone = L.marker([r["lat"],r["longi"]], {icon: icone}).addTo(map);
    layerLieux.addLayer(icone);
  })
};
