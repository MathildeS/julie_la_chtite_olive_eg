var map = L.map('mapid').setView([46.52863469527167,2.43896484375], 6);

var test2 = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
maxZoom: 20,
subdomains:['mt0','mt1','mt2','mt3']
}).addTo(map);


var ObjetFixe = L.Icon.extend({
  options: {
  iconSize: [40, 40],
  iconAnchor: null,
  popupAnchor: [4, 4],
  maxZoom : 15,
  }
});

// function extractionLieu(nom){
//   var tableau_donnees = {cheminacces: '', lat: 0, longi: 0};
//   var data = {type: "creationLieu", demandeur: nom};
//
//   fetch('essai_php.php', {
//     method: 'post',
//     body: JSON.stringify(data)
//   })
//   .then(r => r.json()
//   )
//   .then(r => {
//     // console.log(r);
//     tableau_donnees["cheminacces"] = r["cheminacces"];
//     tableau_donnees["lat"] = r["lat"];
//     tableau_donnees["longi"] = r["longi"];
//   })
//   return tableau_donnees;
// }
//
// function creationLieu(tableau_donnees){
//   console.log(tableau_donnees);
//   marker_icon = new ObjetFixe({iconUrl: tableau_donnees["cheminacces"]});
//   marker = L.marker([tableau_donnees["lat"],tableau_donnees["longi"]], {icon: marker_icon});
//   console.log(marker);
//   return marker;
// }

// Création des icônes des lieux
// olivier = creationLieu(extractionLieu("forcalquier")).addTo(map);
// console.log(olivier);
olivier = new ObjetFixe({iconUrl: 'lieux/olivier.png'});
mont_blanc_gateau = new ObjetFixe({iconUrl: 'objets/mont_blanc.png'});
charbon = new ObjetFixe({iconUrl: 'objets/charbon.png'});
canele = new ObjetFixe({iconUrl: 'objets/canele_png.png'});
mont_dor = new ObjetFixe({iconUrl: 'objets/mont_dor.jpg'});

// Création des marqueurs des lieux
olivier = L.marker([43.959933,5.780712], {icon: olivier}).addTo(map);
mont_blanc_gateau = L.marker([45.832622,6.865175], {icon: mont_blanc_gateau});
charbon = L.marker([50.460844, 2.986978], {icon: charbon});
canele = L.marker([44.840118,-0.598408], {icon: canele});
mont_dor = L.marker([45.528387,2.813930], {icon: mont_dor});


var nom_et_chemin_lieux = importNomCheminLieu();
var nom_et_chemin_objet = importNomCheminObjet();

function importNomCheminLieu(){
  var nom_et_chemin = [];
  var data = {type: "importNomCheminL", demandeur: "lieux"};

  fetch('essai_php.php',{
    method: 'post',
    body: JSON.stringify(data)
  })
  .then(r => r.json()
)
  .then(r => {
    for (var i =0; i<= r.length-1; i++){
      nom_et_chemin.push(r[i]);
    };
  })
  return nom_et_chemin;
}

function importNomCheminObjet(){
  var nom_et_chemin = [];
  var data = {type: "importNomCheminO", demandeur: "objets"};

  fetch('essai_php.php',{
    method: 'post',
    body: JSON.stringify(data)
  })
  .then(r => r.json()
)
  .then(r => {
    for (var i =0; i<= r.length-1; i++){
      nom_et_chemin.push(r[i]);
    };
  })
  return nom_et_chemin;
}


function trouverIndice(nom, nom_et_chemin_lieux){
  var ind = 0;
  for (var i = 0; i<= nom_et_chemin_lieux.length-1; i++){
    if (nom_et_chemin_lieux[i]["nom"]==nom) {
      ind = i;
    }
  }
  return ind;
}


function creationMarker(ind, nom_chemin){
  marker_icon = new  ObjetFixe({iconUrl: nom_chemin[ind]["cheminacces"]});
  marker = L.marker([nom_chemin[ind]["lat"],nom_chemin[ind]["longi"]], {icon: marker_icon});
  return marker;
}

var layerLieux = L.layerGroup();
var layerObjets = L.layerGroup();
var paraind = document.getElementById('paraind');
var form = document.getElementById('valid_finale');
var reponse = document.getElementById('reponse');
var validerep = document.getElementById('validerep');

function ajoutLieuLayer(lieu){
  layerLieux.addLayer(lieu);
}

function ajoutObjetLayer(objet){
  layerObjets.addLayer(objet);
}


map.on('zoomend', function(){
  var zoomlevel = map.getZoom();
  if (zoomlevel < 10){
    if (map.hasLayer(layerLieux)) {
      map.removeLayer(layerLieux)
    }else {
      console.log("no");
      console.log(zoomlevel);
    }
    if (map.hasLayer(layerObjets)) {
      map.removeLayer(layerObjets)
    }else {
      console.log("no");
      console.log(zoomlevel);
    }
  }
  if (zoomlevel>=10) {
    console.log(zoomlevel);
    if (map.hasLayer(layerLieux)) {
      console.log("lieux already add");
    }else {
      layerLieux.addTo(map);
    }
    if (map.hasLayer(layerObjets)) {
      console.log("objets already add");
    }else {
      layerObjets.addTo(map);
    }
  }

})

olivier.addEventListener('click', function(){
  ind_mt_l = trouverIndice("mont-lure", nom_et_chemin_lieux);
  mont_lure = creationMarker(ind_mt_l, nom_et_chemin_lieux);
  ajoutLieuLayer(mont_lure);

  mont_lure.addEventListener('click',function(){
    ind_gent = trouverIndice("gentiane", nom_et_chemin_objet);
    gentiane = creationMarker(ind_gent, nom_et_chemin_objet);
    ajoutObjetLayer(gentiane);

    gentiane.addEventListener('click', function(){
      var popupContent = '<p>H : Oh mais il y a là une belle gentiane ! Je vais la ramener à Julie ! Tient, il y a un petit parchemin sous la gentiane… Qu’y a-t-il décrit dessus ?</p><p>"Pour voir plus loin, il faut aller sur le toit de la France"</p>';
      gentiane.bindPopup(popupContent).openPopup();
      ind_mt_blc = trouverIndice("mont-blanc", nom_et_chemin_lieux);
      mont_blanc = creationMarker(ind_mt_blc, nom_et_chemin_lieux);
      ajoutLieuLayer(mont_blanc);
      paraind.innerHTML = "Pour voir plus loin, il faut aller sur le toit de la France";

      mont_blanc.addEventListener('click', function(){
        ind_coffre1 = trouverIndice("coffre1", nom_et_chemin_objet);
        coffre1 = creationMarker(ind_coffre1, nom_et_chemin_objet);
        ajoutObjetLayer(coffre1);

        coffre1.addEventListener('click', function(){
          var enigme = "Enigme du coffre: A quelle hauteur suis-je ? ";
          var valeur = 4810;
          var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
          while (true){
            if (suggestion != valeur) {
              suggestion = prompt('Raté, veuillez réessayer !');
            }else {
              alert('Bonne réponse');
              ajoutObjetLayer(mont_blanc_gateau);
              break;
            }
          }
        })
      })
    })
  })
})

mont_blanc_gateau.addEventListener('click', function(){
  paraind.innerHTML = "Quand on a une vue très perçante, on peut apercevoir mon unique clocher qui culmine à 141m.";
  var popupContent = "<p>Voici l'indice suivant inscrit au fond du coffre :</p><p>Quand on a une vue très perçante, on peut apercevoir mon unique clocher qui culmine à 141m.</p>";
  mont_blanc_gateau.bindPopup(popupContent).openPopup();
  ind_strb = trouverIndice("strasbourg", nom_et_chemin_lieux);
  strasbourg = creationMarker(ind_strb, nom_et_chemin_lieux);
  console.log(strasbourg);
  ajoutLieuLayer(strasbourg);

  strasbourg.addEventListener('click', function(){
    ind_bretzel = trouverIndice("bretzel", nom_et_chemin_objet);
    bretzel = creationMarker(ind_bretzel, nom_et_chemin_objet);
    ajoutObjetLayer(bretzel);

    bretzel.addEventListener('click', function(){
       var popupContent = '<p>En haut du clocher Hatlas croise un coléoptère.</p><p>C : Bien le bonjour à toi papillon, si tu cherches un endroit à visiter je te conseillerai cette destination !</p><p>Le coléoptère donne à Hatlas un parchemin sur lequel il est écrit :</p><p>Pour une destination de rêve se rendre, dans les profondeurs au nord de la France à l’endroit où les hommes ont extrait pour la dernière fois les entrailles de la Terre dans cette région.</p>';
       bretzel.bindPopup(popupContent).openPopup();
       ind_nord = trouverIndice("nord", nom_et_chemin_lieux);
       nord = creationMarker(ind_nord, nom_et_chemin_lieux);
       ajoutLieuLayer(nord);
       paraind.innerHTML = "Pour une destination de rêve se rendre, dans les profondeurs au nord de la France à l’endroit où les hommes ont extrait pour la dernière fois les entrailles de la Terre dans cette région.";

       nord.addEventListener('click', function(){
         ind_coffre2 = trouverIndice("coffre2", nom_et_chemin_objet);
         coffre2 = creationMarker(ind_coffre2, nom_et_chemin_objet);
         ajoutObjetLayer(coffre2);

         coffre2.addEventListener('click', function(){
           var enigme = "Enigme du coffre: En quelle année, la mine d'Oignies a-t-elle été fermée ? ";
           var valeur = 1990;
           var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
           while (true){
             if (suggestion != valeur) {
               suggestion = prompt('Raté, veuillez réessayer !');
             }else {
               alert('Bonne réponse');
               ajoutObjetLayer(charbon);
               break;
             }
           }
         })
       })
    })
  })
})

charbon.addEventListener('click', function(){
  ind_bretagne = trouverIndice("bretagne", nom_et_chemin_lieux);
  bretagne = creationMarker(ind_bretagne, nom_et_chemin_lieux);
  ajoutLieuLayer(bretagne);
  var popupContent = "<p>Dans ce coffre il y avait de beaux morceaux de charbons et une vieille lettre adressée à un mineur sur laquelle était inscrit :</p><p>Mon environnement est maintenant composé de roches roses, il n'est plus noir comme le tien... Je te conseille vivement de m'y rejoindre, l'air y est bien plus respirable mais le temps y est très changeant !</p>";
  charbon.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Mon environnement est maintenant composé de roches roses, il n'est plus noir comme le tien... Je te conseille vivement de m'y rejoindre, l'air y est bien plus respirable mais le temps y est très changeant !";

  bretagne.addEventListener('click', function(){
    ind_gateau_bre = trouverIndice("gateau breton", nom_et_chemin_objet);
    gateau_breton = creationMarker(ind_gateau_bre, nom_et_chemin_objet);
    ajoutObjetLayer(gateau_breton);

    gateau_breton.addEventListener('click', function(){
      ind_paris = trouverIndice("paris", nom_et_chemin_lieux);
      tour_eiffel = creationMarker(ind_paris, nom_et_chemin_lieux);
      ajoutLieuLayer(tour_eiffel);
      var popupContent = "<p>Hatlas entend une conversation entre deux libellules :</p><p>L1 : Ce bâtiment est le plus de haut de notre belle capitale !</p><p>L2 : Oui, c’est le plus impressionnant que j’ai vu !</p><p>H : *Tiens, tiens, je crois que je vais me rendre à la capitale voir ce fameux monument*</p>";
      gateau_breton.bindPopup(popupContent).openPopup();
      paraind.innerHTML = "Rendez-vous sur le plus haut monument de la capitale française.";



      tour_eiffel.addEventListener('click', function(){

        ind_opera = trouverIndice("opera", nom_et_chemin_objet);
        opera = creationMarker(ind_opera, nom_et_chemin_objet);
        ajoutObjetLayer(opera);

        ind_bordeaux = trouverIndice("bordeaux", nom_et_chemin_lieux);
        bordeaux = creationMarker(ind_bordeaux, nom_et_chemin_lieux);
        ajoutLieuLayer(bordeaux);

        var popupContent = "<p>Hatlas se rend en haut de la tour Eiffel et voit un couple manger un superbe gâteau.</p><p>F : Humm !!! Cet opéra est délicieux !</p><p>Ho : Oui et ce petit vin était vraiment excellent !</p><p>F : Oui, il provient d’une ville très connue pour son vin !</p><p>H : *Hum, je vais emporter un bout de ce gâteau pour Julie et après je vais me rendre dans cette fameuse ville*</p><p>*Pensez a prendre l'opéra pour Julie*</p>";
        tour_eiffel.bindPopup(popupContent).openPopup();
        paraind.innerHTML = "N'oubliez pas de prendre l'opera puis rendez-vous dans une ville connue essentiellement pour son vin.";

        bordeaux.addEventListener('click', function(){
          ind_coffre3 = trouverIndice("coffre3", nom_et_chemin_objet);
          coffre3 = creationMarker(ind_coffre3, nom_et_chemin_objet);
          ajoutObjetLayer(coffre3);

          coffre3.addEventListener('click', function(){
            var enigme = "Enigme du coffre: code INSEE de cette grande ville.";
            var valeur = 33063;
            var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
            while (true){
              if (suggestion != valeur) {
                suggestion = prompt('Raté, veuillez réessayer !');
              }else {
                alert('Bonne réponse');
                ajoutObjetLayer(canele);
                break;
              }
            }
          })
        })
      })
    })
  })
})

canele.addEventListener('click', function(){
  ind_TB = trouverIndice("tire-bouchon", nom_et_chemin_objet);
  tire_bouchon = creationMarker(ind_TB, nom_et_chemin_objet);
  ajoutObjetLayer(tire_bouchon);

  tire_bouchon.addEventListener('click', function(){
    ind_digoin = trouverIndice("bourgogne", nom_et_chemin_lieux);
    digoin = creationMarker(ind_digoin, nom_et_chemin_lieux);
    ajoutLieuLayer(digoin);
    var popupContent = "<p>Dans le coffre de la tenancière de l'épicierie, il y aviat là de drôle de trouvailles pour notre cher Hatlas. Des canelés qu'elle gardait pour son goûter, un drôle d'engin que l'on appelle 'tire-bouchon' et un prospectus sur lequel trônait une grosse inscription :</p><p>Vous voulez découvrir un mets exceptionnel ? Pour cela rendez vous dans la ville où se déroule la plus grande fête du cousin de la limace.</p><p>Hatlas :*Quel est donc ce drôle d'animal ?*</p>";
    tire_bouchon.bindPopup(popupContent).openPopup();
    paraind.innerHTML = "Vous voulez découvrir un mets exceptionnel ? Pour cela rendez vous dans la ville où se déroule la plus grande fête du cousin de la limace.";

    digoin.addEventListener('click', function(){
      ind_escargot = trouverIndice("escargot", nom_et_chemin_objet);
      escargot = creationMarker(ind_escargot, nom_et_chemin_objet);
      ajoutObjetLayer(escargot);

      ind_vin = trouverIndice("vin", nom_et_chemin_objet);
      vin = creationMarker(ind_vin, nom_et_chemin_objet);
      ajoutObjetLayer(vin);

      vin.addEventListener('click', function(){
        ind_sancy = trouverIndice("sancy", nom_et_chemin_lieux);
        sancy = creationMarker(ind_sancy, nom_et_chemin_lieux);
        ajoutLieuLayer(sancy);
        var popupContent = "<p>Hatlas:*J'ai bien fait de prendre ce tire_bouchon à Bordeaux ainsi je pourrais déboucher cette bouteille! Je vais prendre aussi quelques escargots pour Julie.*</p><p>Hatlas : Il me reste de la place que pour un dernier souvenir et il faut que je reprenne de la hauteur pour voir le chemin pour rentrer. J’ai entendu dire que le point culminant de la chaîne montagneuse du centre de la France valait le détour !</p>";
        vin.bindPopup(popupContent).openPopup();
        paraind.innerHTML = "Hatlas : Il me reste de la place que pour un dernier souvenir et il faut que je reprenne de la hauteur pour voir le chemin pour rentrer. J’ai entendu dire que le point culminant de la chaîne montagneuse du centre de la France valait le détour !";

        sancy.addEventListener('click', function(){
          ind_coffre4 = trouverIndice("coffre4", nom_et_chemin_objet);
          coffre4 = creationMarker(ind_coffre4, nom_et_chemin_objet);
          ajoutObjetLayer(coffre4);

          coffre4.addEventListener('click', function(){
            var enigme = "Enigme du coffre: Quel est le nombre de volcans en Auvergne?";
            var valeur = 80;
            var suggestion = prompt(enigme + "Veuillez rentrer votre réponse :");
            while (true){
              if (suggestion != valeur) {
                suggestion = prompt('Raté, veuillez réessayer !');
              }else {
                alert('Bonne réponse');
                ajoutObjetLayer(mont_dor);
                break;
              }
            }
          })
        })
      })
    })
  })
})

mont_dor.addEventListener('click', function(){
  ind_julie = trouverIndice("julie", nom_et_chemin_objet);
  julie_mure = creationMarker(ind_julie, nom_et_chemin_objet);
  ajoutLieuLayer(julie_mure);
  var popupContent = "<p>Hatlas: Il est maintenant temps de rentrer si je veux revenir avant l'olivaison. Direction le lieu où des élèves de l’ENSG arrivent en grand nombre vers la fin de l’année scolaire</p>";
  mont_dor.bindPopup(popupContent).openPopup();
  paraind.innerHTML = "Direction le lieu où des élèves de l’ENSG arrivent en grand nombre vers la fin de l’année scolaire";

  julie_mure.addEventListener('click', function(){
    form.submit();
  })
})














// // fonction pour récupérer l'objet bloqué par le lieu pris en entrée
// function lieuRecupObjet(val, lieuPrObjet, icon_donnees){
//   var data = {type: lieuPrObjet, demandeur: val};
//
//   fetch('essai_php.php',{
//     method: 'post',
//     body: JSON.stringify(data)
//   })
//   .then(r => r.json()
// )
//   .then(r => {
//     icon_donnees.nom = r["nom"];
//     icon_donnees.cheminacces = r["cheminacces"];
//     icon_donnees.lat = r["lat"];
//     icon_donnees.longi = r["longi"];
//   })
// }
//
// // fonction pour récupérer le lieu bloqué par le lieu pris en entrée
// function lieuRecupLieu(val, lieuPrLieu, icon_donnees){
//   var data = {type: lieuPrLieu, demandeur: val};
//
//   fetch('essai_php.php',{
//     method: 'post',
//     body: JSON.stringify(data)
//   })
//   .then(r => r.json()
//   )
//   .then(r => {
//     icon_donnees.nom = r["nom"];
//     icon_donnees.cheminacces = r["cheminacces"];
//     icon_donnees.lat = r["lat"];
//     icon_donnees.longi = r["longi"];
//   })
// }
//
// // fonction pour récupérer le lieu par l'objet pris en entrée
// function ObjetRecupLieu(val, objetPrLieu, icon_donnees){
//   var data = {type: objetPrLieu, demandeur: val};
//
//   fetch('essai_php.php',{
//     method: 'post',
//     body: JSON.stringify(data)
//   })
//   .then(r => r.json()
// )
//   .then(r => {
//     icon_donnees.nom = r["nom"];
//     icon_donnees.cheminacces = r["cheminacces"];
//     icon_donnees.lat = r["lat"];
//     icon_donnees.longi = r["longi"];
//   })
// }
//
// // fonction pour récupérer l'objet par l'objet pris en entrée
// function ObjetRecupObjet(val, objetPrObjet, icon_donnees){
//   var data = {type: objetPrObjet, $demandeur: val};
//
//   fetch('essai_php.php',{
//     method: 'post',
//     body: JSON.stringify(data)
//   })
//   .then(r => r.json()
// )
//   .then(r => {
//     icon_donnees.nom = r["nom"];
//     icon_donnees.cheminacces = r["cheminacces"];
//     icon_donnees.lat = r["lat"];
//     icon_donnees.longi = r["longi"];
//   })
// }
