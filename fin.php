<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style_fin.css">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Satisfy&display=swap" rel="stylesheet">
    <title>Escape Game</title>
  </head>
  <body>
    <embed src="musiques/julie_la_petite_olive_fin.mp3" autostart="true" loop="false" hidden="true"></embed>
    <p id="heure"; style: "display: none">00:00:00</p>
    <form id="fin" action="score.php" method="post">
      <div id="fin2">
        <p id="fin_hist">Fin de l'histoire</p>
        <p id="deb">Afin de profiter de la fin de cette chanson, veuillez appuyer sur play</p>
        <p>Ainsi Julie pu recevoir sa récompense faire un véritable festin avec toutes les denrées rapportées par Hatlas qu'elle remercie infiniment.</p>
        <p>L'olivaison est finalement arrivée mais Julie a pris beaucoup de tour de taille entre la lumière du soleil emmagasinée durant son apprentissage avec se dernier et son festin avec les souvenirs de Hatlas. L’agriculteur, étant impressionné par sa grosseur, a décidé de l’exposer au salon de l’agriculture, ce qui lui a permis de beaucoup voyager et donc de découvrir par elle-même les merveilles que lui a décrit Hatlas. Ce qui lui fit de magnifiques souvenirs.</p>
        <p id="felicitation">Toutes nos félicitations pour avoir résolu les énigmes et sauvé Julie !!!</p>
        <p class="score"><input id="valid" class="score" type="submit" name="score" value="Scores"></p>
      </div>
    </form>
    <?php
    // Ouverture de la session
    session_start();

    // Connexion à la BDD
    include('connect.php');
    $json = json_decode(file_get_contents('php://input'), true);
    $heure = $json["heure"];
    // Récupération du pseudo dans la session
    $pseudo = $_SESSION["pseudo"];
    $req ="UPDATE classement SET temps_fin = '$heure' WHERE pseudo LIKE '$pseudo' ";
    $result = mysqli_query($link, $req);

    // Requête qui permet de récupérer l'heure de début et de fin du jeu
    if ($result2 = mysqli_query($link, "SELECT temps_debut,temps_fin FROM classement WHERE pseudo LIKE '$pseudo'")) {
      while ($ligne = mysqli_fetch_assoc($result2)) {
        $tableau2[]= $ligne;
      }
    }
    foreach ($tableau2 as $key){
      $debut = $key['temps_debut'];
      $fin = $key['temps_fin'];
    }

    // Partie de calcul du score qui est le temps de joueur

    // Séparation des chaînes de caractères par les :
    $begin = explode(":",$debut);
    $end = explode(":",$fin);

    // Convertit les heures en secondes
    $h_begin_s = floatval($begin[0])*3600;
    $h_end_s = floatval($end[0])*3600;

    //Convertit les minutes en secondes
    $m_begin_s = floatval($begin[1])*60;
    $m_end_s = floatval($end[1])*60;

    // Somme l'ensemble des secondes
    $s_begin = $h_begin_s+$m_begin_s+floatval($begin[2]);
    $s_end = $h_end_s+$m_end_s+floatval($end[2]);

    // Fait la différence des deux heures début et fin de jeu toujours en sec
    $s_diff = abs($s_begin-$s_end);

    // Tests :

    if($s_diff>3600){
      $h = (int)($s_diff/3600);
      $calc = (int)(fmod($s_diff,3600.0)/60);
      if($calc==0){
        $m = "00";
      }else{
        $m = (int)(fmod($s_diff,3600.0)/60);
      }
      $s = fmod($s_diff,60.0);
    }
    elseif ($s_diff==3600) {
      $h = "01";
      if(fmod($s_diff,3600.0)%60==0){
        $m = "00";
      }else{
        $m = (int)(fmod($s_diff,3600.0)/60);
      }
      $s = fmod($s_diff,60.0);
    }
    else {
      $h = "00";
      if($s_diff>60){
        $m = (int)(fmod($s_diff,3600.0)/60);
    }elseif ($s_diff==60) {
      $m = "01";
    }
    else{
      $m = "00";
    }
    $s = fmod($s_diff,60.0);
    }

    // Concaténation pour former le type time 00:00:00
    $score = strval($h).":".strval($m).":".strval($s);

    // Requête pour implémenter les scores
    $req3 ="UPDATE classement SET score = '$score' WHERE pseudo LIKE '$pseudo' ";
    $result3 = mysqli_query($link, $req3);

    ?>

    <script type="text/javascript">
    var heure = document.getElementById("heure");
    function fin_heure(){
      var date = new Date().toLocaleTimeString('fr');
      setTimeout(fin_heure, 1000);
      heure.innerHTML = date;
    }
    fin_heure();
    var txt = heure.innerHTML;
    Recuperer_heure_fin(txt);

    function Recuperer_heure_fin( element2){
      var data = {"heure": element2};
      fetch('fin.php', {
        method:  'post',
        body : JSON.stringify(data)
    });
    }
    </script>
  </body>
</html>
