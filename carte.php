<?php

// Connexion à la BDD
include('connect.php');



// obtention du type de demande et du demandeur
$json = json_decode(file_get_contents('php://input'), true);
$type = $json["type"];
$demandeur = $json["demandeur"];


//récupération du lieu débloqué par le lieu en entrée si il existe
if ($type == "lieuPrLieu") {

  $sql = "SELECT id FROM lieux WHERE nom='$demandeur'";
  $result = mysqli_query($link, $sql);
  $val = 0;
  if($result){
    $val = mysqli_fetch_assoc($result)["id"];
  };

  $sql2 = "SELECT nom,cheminacces,lat,longi,zoom,debloque FROM lieux WHERE bloquepar='".$val."'";
  $result2 = mysqli_query($link, $sql2);
  $donnes_lieu = [];
  if($result2){
    $donnes_lieu = mysqli_fetch_assoc($result2);
  }
  echo json_encode($donnes_lieu);
}


//récupération de l'objet débloqué par le lieu en entrée si il existe
if ($type == "lieuPrObjet") {

  $sql = "SELECT id FROM lieux WHERE nom='$demandeur'";
  $result = mysqli_query($link, $sql);
  $val = 0;
  if($result){
    $val = mysqli_fetch_assoc($result);
  };

  $sql2 = "SELECT nom,lat,longi,zoom,debloque,texte, cheminacces FROM objets WHERE bloquepar='".$val."'";
  $result2 = mysqli_query($link, $sql2);
  $donnes_objet = [];
  if($result2){
   $donnes_objet = mysqli_fetch_assoc($result2);
  }
  echo json_encode($donnes_objet);
}


//récupération du lieu débloqué par l'objet en entrée si il existe
if ($type == "objetPrLieu") {

  $sql = "SELECT id FROM objet WHERE nom='$demandeur";
  $result = mysqli_query($link, $sql);
  $val = 0;
  if($result){
    $val = mysqli_fetch_assoc($result);
  };

  $sql2 = "SELECT nom,cheminacces,lat,longi,zoom,debloque FROM lieux WHERE bloquepar='".$val."'";
  $result2 = mysqli_query($link, $sql2);
  $donnes_lieu = [];
  if($result2){
    $donnes_lieu = mysqli_fetch_assoc($result2);
  }
  echo json_encode($donnes_lieu);
}


//récupération de l'objet débloqué par l'objet en entrée si il existe
if ($type == "objetPrObjet") {

  $sql = "SELECT id FROM objet WHERE nom='$demandeur";
  $result = mysqli_query($link, $sql);
  $val = 0;
  if($result){
    $val = mysqli_fetch_assoc($result);
  };

  $sql2 = "SELECT nom,lat,longi,zoom,debloque,texte, cheminacces FROM objets WHERE bloquepar='".$val."'";
  $result2 = mysqli_query($link, $sql2);
  $donnes_objet = [];
  if($result2){
    $donnes_objet = mysqli_fetch_assoc($result2);
  }
  echo json_encode($donnes_objet);
}

?>
