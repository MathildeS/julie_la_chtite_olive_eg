<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Escape Game : Julie</title>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
   integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
   crossorigin=""></script>
  <link rel="stylesheet" href="css/carte_css.css">
  </head>
  <body>
    <div id="mapid">
    </div>
    <p id="heure"; style: "display: none;">00:00:00</p>
    <div id="menu">
        <div id="indices">
          <legend id="legind">Indices :</legend>
          <p class="ind">Voici votre indice actuel :</p>
          <p id="paraind" class="ind">Hatlas : Je vais commencer par prendre de la hauteur en me rendant sur le point culminant proche de Forcalquier, afin d’avoir une vue panoramique de ce qui m’entoure.</p>
          <p class="ind">Indication :</p>
          <p id="indication" class="ind">Quand vous voyez un icone tel que l'olivier, cliquez dessus pour débloquer la suite.</p>
        </div>
    </div>
    <form id="valid_finale" action="question_finale.php" method="post"></form>
    <div id="espace">
      <p>Inventaire :</p>
      <p></p>
    </div>
    <div id="inventaire">
      <p id='bloc1'></p>
      <p id='bloc2'></p>
      <p id='bloc3'></p>
      <p id='bloc4'></p>
      <p id='bloc5'></p>
      <p id='bloc6'></p>
      <p id='bloc7'></p>
      <p id='bloc8'></p>
      <p id='bloc9'></p>
      <p id='bloc10'></p>
      <p id='bloc11'></p>
      <p id='bloc12'></p>
    </div>
    <?php
    // OUverture session
    session_start();
    // Connexion à la BDD
    include('connect.php');
    $json = json_decode(file_get_contents('php://input'), true);
    $heure = $json["heure"];
    // Récupération du pseudo dans la session
    $pseudo = $_SESSION['pseudo'];
      $req ="UPDATE classement SET temps_debut = '$heure' WHERE pseudo LIKE '$pseudo' ";
      $result = mysqli_query($link, $req);
    ?>
    <script type="text/javascript">
    var heure = document.getElementById("heure");

    // Fonction qui lance l'heure : nous allons stocker l'heure à laquelle cette page s'est chargée
    // dans la BDD
    function debute_heure(){
      var date = new Date().toLocaleTimeString('fr');
      setTimeout(debute_heure, 1000);
      heure.innerHTML = date;
    }

    debute_heure();
    var txt = heure.innerHTML;
    Stockage_heure_debut(txt);

    function Stockage_heure_debut( element2){
      var data = {"heure": element2};
      fetch('carte_escape_game.php', {
        method:  'post',
        body : JSON.stringify(data)
    });
    }
    </script>
    <script type="text/javascript" src="carte_js/escapegame_js.js"></script>
  </body>
</html>
