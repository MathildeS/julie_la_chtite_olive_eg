<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style_introVersMap.css">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Satisfy&display=swap" rel="stylesheet">
    <title>Escape Game</title>
  </head>
  <body>
    <form id="transi" action="escapegameMSMLB.php" method="post">
      <div id="transition">
        <legend id="leg">Prêt ?</legend>
        <p class="narration">Vous allez maintenant incarner Hatlas dans son voyage à la découverte de ce magnifique territoire qu’est la France ! N’oubliez pas que vous rapporterez certains objets pour Julie. Mais attention, il vous faudra résoudre les énigmes en moins de 10min pour que Hatlas soit de retour avant l’olivaison !</p>
        <p class="narration">Pour que le voyage puisse commencer voici votre premier indice :</p>
        <p class="hatlas">Hatlas : Je vais commencer par prendre de la hauteur en me rendant sur le point culminant proche de Forcalquier, afin d’avoir une vue panoramique de ce qui m’entoure.</p>
        <p id="depart"><input class="depart" type="submit" name="depart" value="C'est parti !"></p>
      </div>
    </form>
  </body>
</html>
